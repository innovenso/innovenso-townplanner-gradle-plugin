package com.innovenso.townplan.gradle.plugin

import org.apache.commons.io.FileUtils
import org.gradle.testkit.runner.GradleRunner
import spock.lang.Ignore
import spock.lang.Specification
import spock.lang.TempDir
import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class TownPlanPluginSpec extends Specification {
	@TempDir File testProjectDir
	File buildFile
	File outputDir

	def setup() {
		final File sourceDir = new File(testProjectDir, 'src/main/groovy/townplan')
		sourceDir.mkdirs()
		copySample('zone.sample', new File(sourceDir,'SampleZone.groovy'))
		outputDir = new File(testProjectDir, 'output')
		buildFile = new File(testProjectDir, 'build.gradle')
		buildFile << """
            plugins {
                id 'com.innovenso.townplanner'
            }
            
        """
	}

	def "can render a full townplan as SVG"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('svg')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-svg.zip'}
		result.task(":svg").outcome == SUCCESS
	}

	def "can render a single element as SVG"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('svg', '-Pconcept=a_system')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		final File systemDir = new File(outputDir, 'svg/system')
		systemDir.listFiles().find {it.name == 'a_system-as-is-today.svg'}
		!outputDir.listFiles().find {it.name == 'townplan-svg.zip'}
		result.task(":svg").outcome == SUCCESS
	}

	def "can export town plan to Excel"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('excel')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-excel.zip'}
		result.task(":excel").outcome == SUCCESS
	}

	def "can export town plan to Archimate"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('archimate')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-archimate.zip'}
		result.task(":archimate").outcome == SUCCESS
	}

	@Ignore
	def "can render a full townplan to Blender"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('blender')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-blender.zip'}
		result.task(":blender").outcome == SUCCESS
	}

	@Ignore
	def "can render a full townplan to Confluence"() {
		given:
		when:
		buildFile << """

			confluence {
				apiUrl = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_URL')
				user = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_USER')
				apiKey = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_APIKEY')
				confluenceSpaces = Arrays.asList(System.getenv('TOWNPLAN_WRITER_CONFLUENCE_SPACES').split(","))
				url = System.getenv('TOWNPLAN_WRITER_CONFLUENCE_BASEURL')
			}
"""
		println buildFile.text
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('confluence')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-confluence markup.zip'}
		result.task(":confluence").outcome == SUCCESS
	}

	def "can render a full townplan to LaTeX"() {
		when:
		buildFile << """
			latex {
				theme = 'innovenso'
				institution = 'Test'
			}
"""
		def result = GradleRunner.create().withProjectDir(testProjectDir).withArguments('latex').withPluginClasspath().build()

		then:
		println result.output
		outputDir.listFiles().find { it.name == 'townplan-latex.zip'}
		result.task(':latex').outcome == SUCCESS
	}

	@Ignore
	def 'can render everything with the all task'() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('all')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		outputDir.listFiles().find {it.name == 'townplan-blender.zip'}
		outputDir.listFiles().find {it.name == 'townplan-excel.zip'}
		outputDir.listFiles().find {it.name == 'townplan-archimate.zip'}
		outputDir.listFiles().find {it.name == 'townplan-svg.zip'}
		outputDir.listFiles().find {it.name == 'townplan-confluence markup.zip'}
		result.task(":all").outcome == SUCCESS
	}

	def "can clean output directory"() {
		when:
		def result = GradleRunner.create()
				.withProjectDir(testProjectDir)
				.withArguments('archimate', 'clean')
				.withPluginClasspath()
				.build()

		then:
		println result.output
		!outputDir.exists()
		result.task(":archimate").outcome == SUCCESS
	}


	private void copySample(String source, File target) {
		if (!target.exists()) {
			final URL templateResource = this.getClass().getClassLoader().getResource("samples/" + source)
			FileUtils.copyURLToFile(templateResource, target)
		}
	}
}
