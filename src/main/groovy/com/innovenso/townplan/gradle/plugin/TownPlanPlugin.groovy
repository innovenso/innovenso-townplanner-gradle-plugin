package com.innovenso.townplan.gradle.plugin

import org.apache.commons.io.FileUtils
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.JavaExec

class TownPlanPlugin implements Plugin<Project> {


	@Override
	void apply(Project project) {
		project.extensions.create('townPlan', TownPlanPluginCoreExtension)
		project.extensions.create('svg', TownPlanPluginSvgExtension)
		project.extensions.create('confluence', TownPlanPluginConfluenceExtension)
		project.extensions.create('latex', TownPlanPluginLatexExtension)

		project.plugins.apply('java-library')
		project.plugins.apply('groovy')
		project.plugins.apply('com.diffplug.spotless')
		project.plugins.apply('org.springframework.boot')
		project.plugins.apply('io.spring.dependency-management')

		project.repositories {
			mavenCentral()
			mavenLocal()
		}

		project.configurations {
			all.collect { configuration ->
				configuration.exclude group: 'org.springframework.boot', module: 'spring-boot-starter-logging'
			}
		}


		project.dependencies {
			implementation 'com.innovenso.townplanner:innovenso-townplanner-gradle-runtime:3.1.1'
			implementation 'org.hibernate.validator:hibernate-validator:7.0.1.Final'
			implementation 'org.glassfish:jakarta.el:4.0.1'
			implementation 'jakarta.validation:jakarta.validation-api:3.0.0'
		}

		project.springBoot {
			mainClass = 'com.innovenso.townplan.gradle.TownPlanRunner'
		}

		project.afterEvaluate {
			registerTask(project, 'svg', 'Render diagrams for this town plan in SVG', [:], 'svg')
			registerTask(project, 'excel', 'Export this town plan to Excel', [:], 'excel')
			registerTask(project, 'archimate', 'Export this town plan to OpenGroup Exchange format, for import in other architecture tools', [:], 'archimate')
			registerTask(project, 'blender', 'Render concepts of this town plan as individual Blender files, for use in 3D animations', [:], 'blender')
			registerTask(project, 'confluence', 'Export this town plan to Confluence Cloud', buildConfluenceEnvironment(project), 'confluence')
			registerTask(project, 'latex', 'Export this town plan to LaTeX files, including Beamer slide decks', buildLatexEnvironment(project), 'latex')

			project.mkdir 'src/main/resources'
			copyTemplate('log4j2.xml', project.file('src/main/resources/log4j2.xml'))

			project.tasks.register('all') {
				dependsOn 'svg', 'excel', 'archimate', 'blender', 'confluence', 'latex'
			}
		}

		project.tasks.register('cleanTownPlan', Delete) {
			it.delete 'output', 'logs'
		}

		project.tasks.getByName('clean').configure {
			dependsOn 'cleanTownPlan'
		}
	}

	private static Map<String,Object> buildConfluenceEnvironment(Project project) {
		def confluence = project.extensions.getByName('confluence')
		[
			'TOWNPLAN_WRITER_CONFLUENCE_URL': confluence.apiUrl,
			'TOWNPLAN_WRITER_CONFLUENCE_USER': confluence.user,
			'TOWNPLAN_WRITER_CONFLUENCE_APIKEY': confluence.apiKey,
			'TOWNPLAN_WRITER_CONFLUENCE_SPACES': confluence.confluenceSpaces?.join(','),
			'TOWNPLAN_WRITER_CONFLUENCE_MAINMENUNAME': confluence.mainMenuTitle,
			'TOWNPLAN_WRITER_CONFLUENCE_ARCHETYPEPAGEPOSTFIX': confluence.pageTitleSuffix,
			'TOWNPLAN_WRITER_CONFLUENCE_BASEURL': confluence.url,
			'TOWNPLAN_WRITER_CONFLUENCE_SPARX_URL': confluence.sparxUrl
		]
	}

	private static Map<String,Object> buildLatexEnvironment(Project project) {
		def latex = project.extensions.getByName('latex')
		[
			'TOWNPLAN_WRITER_LATEX_THEME': latex.theme,
			'TOWNPLAN_WRITER_LATEX_INSTITUTION': latex.institution
		]
	}

	private static void registerTask(Project project, String name, String taskDescription, Map<String, Object> environmentMap, String springProfiles) {
		project.tasks.register(name, JavaExec) {
			group = "Town Plan"
			description = taskDescription
			classpath = project.sourceSets.main.runtimeClasspath
			environment environmentMap
			mainClass = 'com.innovenso.townplan.gradle.TownPlanRunner'
			setArguments(project, it, springProfiles)
		}
	}

	private static void setArguments(Project project, JavaExec javaExec, String springProfiles) {
		List<String> args = []
		if (project.hasProperty('concept')) {
			args.add "--concept=${project.getProperty('concept')}"
		}
		if (springProfiles) {
			args.add("--spring.profiles.active=${springProfiles}")
		}
		javaExec.setArgs(args)
	}

	private void copyTemplate(String source, File target) {
		if (!target.exists()) {
			final URL templateResource = this.getClass().getClassLoader().getResource("templates/" + source)
			FileUtils.copyURLToFile(templateResource, target);
		}
	}
}
