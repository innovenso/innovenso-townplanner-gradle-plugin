package com.innovenso.townplan.gradle.plugin

class TownPlanPluginConfluenceExtension {
	String apiUrl
	String user
	String apiKey
	List<String> confluenceSpaces
	String mainMenuTitle = 'Architecture'
	String pageTitleSuffix = 'Diagrams'
	String url
	String sparxUrl = ''
}
